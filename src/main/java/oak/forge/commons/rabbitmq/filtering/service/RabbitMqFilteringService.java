package oak.forge.commons.rabbitmq.filtering.service;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class RabbitMqFilteringService {

    public static void main(String[] args) {
        SpringApplication.run(RabbitMqFilteringService.class,args);
    }
}
