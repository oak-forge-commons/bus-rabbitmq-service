package oak.forge.commons.rabbitmq.filtering.service;

import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;

@Getter
@Setter
@ConfigurationProperties(prefix = "rabbitmq")
public class RabbitProperties {

    private final Ssl ssl = new Ssl();
    private String username;
    private String password;
    private String host;
    private int port;

    @Getter
    @Setter
    public static class Ssl {
        private boolean enabled;
        private String keyStore;
        private String keyStorePassword;
        private String trustStore;
        private String trustStorePassword;
    }

}
