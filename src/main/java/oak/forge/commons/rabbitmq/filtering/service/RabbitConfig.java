package oak.forge.commons.rabbitmq.filtering.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import oak.forge.commons.rabbitmq.client.RabbitBusClient;
import oak.forge.commons.rabbitmq.client.RabbitClientFactory;
import oak.forge.commons.rabbitmq.connection.RabbitConfigFactory;
import oak.forge.commons.rabbitmq.connection.RabbitConnectionData;
import oak.forge.commons.rabbitmq.connection.RabbitConnectionFactory;
import oak.forge.commons.rabbitmq.connection.SslConfig;
import oak.forge.commons.rabbitmq.mapper.DefaultObjectMapper;
import oak.forge.commons.rabbitmq.topic.factory.ConfigType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
@EnableConfigurationProperties({RabbitProperties.class})
public class RabbitConfig {

    @Autowired
    RabbitProperties rabbitProperties;

    @Bean
    RabbitConnectionData rabbitConnectionData() {
        return new RabbitConnectionData(rabbitProperties.getUsername(), rabbitProperties.getPassword(),
                rabbitProperties.getHost(), rabbitProperties.getPort(), rabbitProperties.getSsl().isEnabled());
    }

    @Bean
    SslConfig sslConfig() {
        return new SslConfig(rabbitProperties.getSsl().getKeyStorePassword(),
                rabbitProperties.getSsl().getKeyStore(), rabbitProperties.getSsl().getTrustStore());
    }

    @Bean
    RabbitConfigFactory configFactory(RabbitConnectionData connectionData, SslConfig sslConfig) {
        return new RabbitConfigFactory(connectionData, sslConfig);
    }

    @Bean
    RabbitConnectionFactory rabbitConnectionFactory(RabbitConfigFactory rabbitConfigFactory) {
        return new RabbitConnectionFactory(rabbitConfigFactory);
    }

    @Bean
    ObjectMapper objectMapper() {
        return new DefaultObjectMapper();
    }

    @Bean
    RabbitClientFactory rabbitClientFactory(RabbitConnectionFactory rabbitConnectionFactory, ObjectMapper objectMapper) {
        return new RabbitClientFactory(rabbitConnectionFactory, objectMapper);
    }

    @Bean
    RabbitBusClient rabbitBusClient(RabbitClientFactory rabbitClientFactory) {
        return rabbitClientFactory.prepareBusClient(ConfigType.FILTERING);
    }

    @Bean
    RabbitMqFilter messageFilter(RabbitBusClient rabbitBusClient) {
        return new RabbitMqFilter(rabbitBusClient);
    }
}
