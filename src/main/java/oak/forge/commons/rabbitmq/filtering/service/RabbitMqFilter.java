package oak.forge.commons.rabbitmq.filtering.service;

import lombok.extern.slf4j.Slf4j;
import oak.forge.commons.bus.MessageBus;
import oak.forge.commons.data.message.Message;

@Slf4j
public class RabbitMqFilter {

    private final MessageBus messageBus;

    public RabbitMqFilter(MessageBus messageBus) {
        this.messageBus = messageBus;
        messageBus.registerMessageHandler(Message.class, this::handleMessage);
    }

    public void handleMessage(Message message) {
        messageBus.post(message);
        log.info("message posted after filtering");
    }

}
